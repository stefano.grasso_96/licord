import {  NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default.component';
import { DashboardComponent } from 'src/app/modules/dashboard/dashboard.component';
import { RouterModule } from '@angular/router';
import { PostsComponent } from 'src/app/modules/posts/posts.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import {  MatDividerModule } from '@angular/material/divider';
import {  MatCardModule } from '@angular/material/card';
import {  MatPaginatorModule } from '@angular/material/paginator';
import {  MatTableModule } from '@angular/material/table';

import { FlexLayoutModule } from '@angular/flex-layout';
import { DashboardService } from 'src/app/modules/dashboard.service';
import { MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ArticlesComponent } from 'src/app/modules/articles/articles.component';

import { HighchartsChartModule } from 'highcharts-angular';
import { WarningsComponent } from 'src/app/modules/warnings/warnings.component';
import { AddWorksiteComponent } from 'src/app/modules/add-worksite/add-worksite.component';
import { AssignLicordComponent } from 'src/app/modules/assign-licord/assign-licord.component';
import { AssignTechnicianComponent } from 'src/app/modules/assign-technician/assign-technician.component';
import { ModifyUserComponent } from 'src/app/modules/modify-user/modify-user.component';
import { UsersComponent } from 'src/app/modules/users/users.component';
import { LicordListComponent } from 'src/app/modules/licord-list/licord-list.component';
import { FormsModule } from '@angular/forms';
import { ToolsAdminComponent } from 'src/app/modules/tools-admin/tools-admin.component';
import { ModifyLicordComponent } from 'src/app/modules/modify-licord/modify-licord.component';
import { ManageAccountsComponent } from 'src/app/modules/manage-accounts/manage-accounts.component';
import { ManageWorksiteComponent } from 'src/app/modules/manage-worksite/manage-worksite.component';

import { NgtCanvasModule, NgtColorPipeModule, NgtCoreModule } from '@angular-three/core';
import { NgtAmbientLightModule, NgtPointLightModule } from '@angular-three/core/lights';
import { NgtPrimitiveModule } from '@angular-three/core/primitive';
import { NgtSobaLoaderModule } from '@angular-three/soba/loaders';
import { NgtSobaOrbitControlsModule } from '@angular-three/soba/controls';



@NgModule({
  declarations: [
    DefaultComponent,
    DashboardComponent,
    PostsComponent,
    ArticlesComponent,
    WarningsComponent,
    UsersComponent,
    ModifyUserComponent,
    AssignLicordComponent,
    AssignTechnicianComponent,
    AddWorksiteComponent,
    LicordListComponent,
    ToolsAdminComponent,
    ModifyLicordComponent,
    ManageAccountsComponent,
    ManageWorksiteComponent,
   
    
    ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatSidenavModule,
    MatDividerModule,
    FlexLayoutModule,
    MatCardModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    HighchartsChartModule,
    MatCheckboxModule,
    FormsModule,
    NgtCanvasModule,
    NgtSobaLoaderModule,
    NgtPrimitiveModule,
    NgtSobaOrbitControlsModule,
    NgtAmbientLightModule,
    NgtPointLightModule,
    NgtColorPipeModule
    
  ],
  
  providers: [
    DashboardService
  ]
})
export class DefaultModule { }
