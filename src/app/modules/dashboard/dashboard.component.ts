import { Component, OnInit } from '@angular/core';

import { DashboardService } from '../dashboard.service';
import HC_exporting from 'highcharts/modules/exporting';
declare var Highcharts: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  historical: any;
  
  bigChartF1: any;
  bigChartF2: any;
  bigChartF3: any;
  bigChartBattery: any;
<<<<<<< HEAD

  forza1: any;
  Vaforza1 = true;
  

  forza2: any;
  Vaforza2 = true;

  forza3: any;
  Vaforza3 = true;
  

  batteria: any;

  temperatura: any;
  Vatemp = true;
  

  temperatura2: any;
  Vatemp2 = true;
  

  series:any;
  seriesB:any;

  constructor(private dashboardService: DashboardService) {
    
   }
=======
  bigChartT: any;


  constructor(private dashboardService: DashboardService) { }
>>>>>>> parent of 53e8173 (updated dashboard)

  ngOnInit() {
    this.historical = this.dashboardService.historical();    
    this.bigChartBattery = this.dashboardService.bigChartBattery();

    this.createchart(this.historical, 'historical')
    
<<<<<<< HEAD
    this.createchart(this.bigChartBattery, 'batteria')*/
    this.forza1 = this.dashboardService.forza1();
    this.forza2 = this.dashboardService.forza2();
    this.forza3 = this.dashboardService.forza3();
    this.batteria = this.dashboardService.batteria();
    this.temperatura = this.dashboardService.temperatura();
    this.temperatura2 = this.dashboardService.temperatura2();
////////////////////////////////////////
    this.series=[{
      name: 'Forza1',
      data: this.forza1,
      color: '#d7c233',
      legendIndex: 1,
      visible: this.Vaforza1,
      showInNavigator: true
    },
    {
      name: 'Forza2',
      data: this.forza2,
      color: '#df9d2a',
      legendIndex: 1,
      visible: this.Vaforza2,
      showInNavigator: true
    },
    {
      name: 'Forza3',
      data: this.forza3,
      color: '#dd3c23',
      legendIndex: 1,
      visible: this.Vaforza3,
      showInNavigator: true
    },
    {
      name: 'Temperatura',
      data: this.temperatura,
      color: '#39b963',
      legendIndex: 1,
      visible: this.Vatemp,
      showInNavigator: true
    },
    {
      name: 'Temperatura2',
      data: this.temperatura2,
      color: '#319de2',
      legendIndex: 1,
      visible: this.Vatemp2,
      showInNavigator: true
    }]
//////////////////////////////////
    this.seriesB=[{
      name: 'Batteria',
      data: this.batteria,
      color: '#14358b',
      legendIndex: 1,
      showInNavigator: true
    }]

    this.createchart( this.series ,'historical')
    this.createchart( this.seriesB ,'batteria')
=======
    this.createchart(this.bigChartBattery, 'batteria')
    

>>>>>>> parent of 53e8173 (updated dashboard)

    HC_exporting(Highcharts);

    setTimeout(() => {
      window.dispatchEvent(
        new Event('resize')
      );
    }, 300);
  }


  //Common options for charts
  createchart(data: any, a: string) {
    Highcharts.stockChart(a, {

      rangeSelector: {
        //position top default
        inputPosition: {
          align: 'left',
          x: 0,
          y: 0
        },
        buttonPosition: {
          align: 'right',
          x: 0,
          y: 0
        },

      },

      yAxis: {
        opposite: false,

        plotLines: [{
          value: 0,// Value of where the line will appear
          width: 2,

          color: 'silver'
        }]
      },

      plotOptions: {
        series: {
          pointStart: Date.UTC(2022, 0, 1),
          pointInterval: 3600 * 1000 * 24 // one hour
        },

<<<<<<< HEAD
      },      
      series: serie
    });

  }
  
  Vforza1() {
    let flag = document.getElementById('force1');
    this.Vaforza1 = !this.Vaforza1
    if(this.Vaforza1){
      flag!.style.backgroundColor='#d7c233';
    }else{
      flag!.style.backgroundColor='grey';
    }
    this.ngOnInit()
  };
  Vforza2() {
    let flag = document.getElementById('force2');
    this.Vaforza2 = !this.Vaforza2
    if(this.Vaforza2){
      flag!.style.backgroundColor='#df9d2a';
    }else{
      flag!.style.backgroundColor='grey';
    }
    this.ngOnInit()
  };
  Vforza3() {
    let flag = document.getElementById('force3');
    this.Vaforza3 = !this.Vaforza3
    if(this.Vaforza3){
      flag!.style.backgroundColor='#dd3c23';
    }else{
      flag!.style.backgroundColor='grey';
    }
    this.ngOnInit()
  };

  Vtemp() {
    let flag = document.getElementById('temperature');
    this.Vatemp = !this.Vatemp
    if(this.Vatemp){
      flag!.style.backgroundColor='#39b963';
    }else{
      flag!.style.backgroundColor='grey';
    }
    this.ngOnInit()
  };
  Vtemp2() {
    let flag = document.getElementById('temperature2');
    this.Vatemp2 = !this.Vatemp2
    if(this.Vatemp2){
      flag!.style.backgroundColor='#319de2';
    }else{
      flag!.style.backgroundColor='grey';
    }
    this.ngOnInit()
  };

=======
      },

      series: data
    });

  }
>>>>>>> parent of 53e8173 (updated dashboard)

}


