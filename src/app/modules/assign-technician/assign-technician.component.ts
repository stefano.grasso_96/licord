import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

interface User {
  checked: boolean;
  name: string;
  lastname: string;
  mail: string;
  type: string;

}

@Component({
  selector: 'app-assign-technician',
  templateUrl: './assign-technician.component.html',
  styleUrls: ['./assign-technician.component.scss']
})
export class AssignTechnicianComponent implements OnInit {

  dataSource: MatTableDataSource<User>;
  posts: User[] = [];
  columns: string[] = ['checked', 'name', 'lastname', 'mail', 'type'];

  @ViewChild(MatSort, { static: true }) sort: MatSort = new MatSort;
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;
  constructor() {
    this.posts = [{
      checked: false,
      name: 'Daniel',
      lastname: 'Kristeen',
      mail: 'daniel@website.com',
      type: 'Responsabile'
    },
    {
      checked: false,
      name: 'Daniel',
      lastname: 'Kristeen',
      mail: 'daniel@website.com',

      type: 'Responsabile',

    },
    {
      checked: false,
      name: 'Daniel',
      lastname: 'Kristeen',
      mail: 'daniel@website.com',

      type: 'Responsabile',

    },
    {
      checked: false,
      name: 'Daniel',
      lastname: 'Kristeen',
      mail: 'daniel@website.com',

      type: 'Responsabile',

    },
    {
      checked: false,
      name: 'Daniel',
      lastname: 'Kristeen',
      mail: 'daniel@website.com',

      type: 'Responsabile',

    },
    {
      checked: false,
      name: 'Daniel',
      lastname: 'Kristeen',
      mail: 'daniel@website.com',

      type: 'Responsabile',

    }];

    this.dataSource = new MatTableDataSource(this.posts);
  }

  ngOnInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: any){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase()
  }

}
