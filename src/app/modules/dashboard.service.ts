import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor() { }

  licords() {
    return [{
      token: 'eagegwq24rwger',
      battery: '2342',
      forza1: '1',
      forza2: '2',
      forza3: '3',
      temperature: '23',
      cantiere: 'via roma'
    },
    {
      token: 'qrewhryjrseawgw',
      battery: '245',
      forza1: '11',
      forza2: '22',
      forza3: '33',
      temperature: '25',
      cantiere: 'via pisa'
    },
    {
      token: 'wHYYETJFHGAF<S',
      battery: ' 2425',
      forza1: '111',
      forza2: '222',
      forza3: '333',
      temperature: '11',
      cantiere: 'via pisa'
    },
    {
      token: 'dahsetjudyrjah',
      battery: '3532',
      forza1: '1111',
      forza2: '2222',
      forza3: '3333',
      temperature: '32',
      cantiere: 'via ferrara'
    },
    ]
  }
  /*
  series: [{
            name: 'HiddenByDefault',
            legendIndex: 1,
            visible: false,
            color: '#4572A7',
            type: 'spline',
            data: [a, b, c],
            tooltip: {
                valueSuffix: ' ¥'
            }

        }
  
  */

  //Data for charts
  historical(): any[] {
    return [{
      name: 'Forza1',
      data: [4353,346,34,775,86,22,26747,3231,1,63,73,748,7336,121,51,35,236,346,46,2423,51,5,151],
      color: 'red',
      showInNavigator: true
    },
    {
      name: 'Forza2',
      data: [502, 635, 809, 947, 1402, 3634, 5,268,267,47,3231,1,63,73,748,7336,1,423,788,89,9,6],
      showInNavigator: true
    },
    {
      name: 'Forza3',
      data: [9, 7, 2, 1, 1, 6, 4,3534,5436,11,423,788,89,9996,32,800,654,22,990,432,9, 7, 2, 1],
      showInNavigator: true
    },
    {
      name: 'Temperatura',
      data: [2432,546,457,800,654,22,990,432,9, 7, 2, 1, 1, 6, 4,263,78,652,132,1,2432,546,457,800,],
      showInNavigator: true
    }
  ]
     
  }
  
   

  bigChartBattery() {
    return [{
      name: 'Batteria',
      data: [4353,346,34,775,86,22,26747,3231,1,63,73,748,7336,121,51,35,236,346,46,2423,51,5,151],
    }]
  }

  



}
