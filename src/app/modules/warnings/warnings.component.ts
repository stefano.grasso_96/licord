import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2'


interface IPost {
  token: string;
  installatore?: string;
  cliente: string;
  cantiere?: string;
  date?: string;
  status:string;
  warning:any;
}

@Component({
  selector: 'app-warnings',
  templateUrl: './warnings.component.html',
  styleUrls: ['./warnings.component.scss']
})
export class WarningsComponent implements OnInit {

  dataSource: MatTableDataSource<IPost>;
  posts: IPost[] = [];
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;
  columns: string[] = ['token', 'installatore','cliente', 'cantiere', 'date','status', 'warning','delete'];

  

  constructor() {
    this.posts = [{
      token: '1',
      installatore: 'stef',
      cliente: 'Rossi',
      cantiere: 'title',
      date: '2020-02-02 10:10:10',
      status:'Inattivo',
      warning: '/assets/lowbattery.png'      
      
    },
    {
      token: '2',
      installatore: 'stef2',
      cliente: 'Rossi',
      cantiere: 'title2',
      date: '2020-02-03 10:10:10',
      status:'Inattivo',
      warning: '/assets/nosignal.png'
    },
    {
      token: '3',
      installatore: 'stef3',
      cliente: 'Rossi',
      cantiere: 'title3',
      date: '2020-02-03 10:10:10',
      status:'Inattivo',
      warning: '/assets/warning.png'
    },
    {
      token: '4',
      installatore: 'stef4',
      cliente: 'Rossi',
      cantiere: 'title4',
      date: '2020-02-04 10:10:10',
      status:'Inattivo',
      warning: '/assets/nosignal.png'
    },
    {
      token: '5',
      installatore: 'stef5',
      cliente: 'Rossi',
      cantiere: 'title5',
      date: '2020-02-05 10:10:10',
      status:'Inattivo',
      warning: '/assets/lowbattery.png'
    },
    {
      token: '6',
      installatore: 'stef6',
      cliente: 'Rossi',
      cantiere: 'title6',
      date: '2020-02-06 10:10:10',
      status:'Inattivo',
      warning: '/assets/lowbattery.png'
    }];

    this.dataSource = new MatTableDataSource(this.posts);
   }

  ngOnInit(): void {
    
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: any){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase()
  }
  disableW(token:any){
    Swal.fire({
      title:'Sei sicuro?',
      text:'Questo warning verrà disabilitato!',
      icon:'warning',
      showCancelButton:true,
      confirmButtonText:'Si, disabilitalo',
      cancelButtonText:'No, conservalo'
    }).then((result)=>{
      if(result.value){
        Swal.fire(
          'Cancellato!',
          'Warning disabilitato',
          'success'
        )
        //azione da fare
        for(let i=0; i< this.posts.length;i++){
      if(this.posts[i].token == token){
       this.posts.splice(i,1)
       this.ngOnInit()
        return 
      }
    }

      }else if(result.dismiss === Swal.DismissReason.cancel){
        Swal.fire(
          'Azione annullata',
          'Il tuo warning è salvo'
        )
        return
      }
    })
    
    
  }

}
