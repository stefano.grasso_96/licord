import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyLicordComponent } from './modify-licord.component';

describe('ModifyLicordComponent', () => {
  let component: ModifyLicordComponent;
  let fixture: ComponentFixture<ModifyLicordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifyLicordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyLicordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
