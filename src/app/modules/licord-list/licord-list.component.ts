import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import * as THREE from 'three';


interface IPost {
  id:string;
  token: string;
  date?: string;
  status:string;
  warning:string;
  dati:string;
}

@Component({
  selector: 'app-licord-list',
  templateUrl: './licord-list.component.html',
  styleUrls: ['./licord-list.component.scss']
})
export class LicordListComponent implements OnInit {

  

  dataSource: MatTableDataSource<IPost>;
  posts: IPost[] = [];
  columns: string[] = ['id','token','date', 'status','warning','dati'];

  @ViewChild(MatSort, { static: true }) sort: MatSort = new MatSort;
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  constructor() { 
    this.posts = [{
      id:'1',
      token: '1q2tergf422',
      
      date: '2020-02-02 10:10:10',
      status:'Attivo',
      warning:'',
      dati:'/assets/right-arrow.png'
    },
    {
      id:'2',
      token: '2rg43t355t424tf',
      
      date: '2020-02-03 10:10:10',
      status:'Inattivo',
      warning:'/assets/lowbattery.png',
      dati:'/assets/right-arrow.png'
    },
    {
      id:'3',
      token: '345t4g5e4jj6',
      
      date: '2020-02-03 10:10:10',
      status:'Attivo',
      warning:'',
      dati:'/assets/right-arrow.png'
    },
    {
      id:'4',
      token: '4rsge5ewr',
      
      date: '2020-02-04 10:10:10',
      status:'Attivo',
      warning:'',
      dati:'/assets/right-arrow.png'
    },
    {
      id:'5',
      token: '5asefw4g4g4g',
      
      date: '2020-02-05 10:10:10',
      status:'Inattivo',
      warning:'/assets/lowbattery.png',
      dati:'/assets/right-arrow.png'
    },
    {
      id:'6',
      token: '6eafg4f4e3ge5ha',
      
      date: '2020-02-06 10:10:10',
      status:'Attivo',
      warning:'/assets/lowbattery.png',
      dati:'/assets/right-arrow.png'
    }];

    this.dataSource = new MatTableDataSource(this.posts);
  }

  ngOnInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    /////////////
    
           
  }

}
