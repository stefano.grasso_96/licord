import {  NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DefaultModule } from './layouts/default/default.module';
import { LoginComponent } from './login/login.component';
import { ForgotPwComponent } from './forgot-pw/forgot-pw.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { InputsModule } from '@progress/kendo-angular-inputs';













@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ForgotPwComponent,
    NotFoundComponent,
    
    
    
    
    
    
    
    
    
    
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DefaultModule,
    InputsModule
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
