import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForgotPwComponent } from './forgot-pw/forgot-pw.component';
import { DefaultComponent } from './layouts/default/default.component';
import { LoginComponent } from './login/login.component';
import { AddWorksiteComponent } from './modules/add-worksite/add-worksite.component';


import { ArticlesComponent } from './modules/articles/articles.component';
import { AssignLicordComponent } from './modules/assign-licord/assign-licord.component';
import { AssignTechnicianComponent } from './modules/assign-technician/assign-technician.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { LicordListComponent } from './modules/licord-list/licord-list.component';
import { ManageAccountsComponent } from './modules/manage-accounts/manage-accounts.component';
import { ManageWorksiteComponent } from './modules/manage-worksite/manage-worksite.component';
import { ModifyLicordComponent } from './modules/modify-licord/modify-licord.component';
import { ModifyUserComponent } from './modules/modify-user/modify-user.component';
import { PostsComponent } from './modules/posts/posts.component';

import { ToolsAdminComponent } from './modules/tools-admin/tools-admin.component';
import { UsersComponent } from './modules/users/users.component';
import { WarningsComponent } from './modules/warnings/warnings.component';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  {path: 'default', component: DefaultComponent,
    children: [
      {path: 'dashboard', component: DashboardComponent},
      {path: 'posts', component: PostsComponent},
      {path: 'articles', component: ArticlesComponent},
      {path: 'users', component: UsersComponent},
      {path: 'modifyUsers', component: ModifyUserComponent},
      {path: 'licordList', component: LicordListComponent},
      {path: 'warnings', component: WarningsComponent},
      {path: 'addworksite', component: AddWorksiteComponent},
      {path: 'assignlicord', component: AssignLicordComponent},
      {path: 'assigntechnician', component: AssignTechnicianComponent},
      {path: 'toolsAdmin', component: ToolsAdminComponent},
      {path: 'modifyLicord', component: ModifyLicordComponent},
      {path: 'manageAccounts', component: ManageAccountsComponent},
      {path: 'manageWorksite', component: ManageWorksiteComponent},
      

    ]},
  {path:'forgot-pw', component: ForgotPwComponent},
  {path:'', component: LoginComponent},
  {path:'**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
